#include <iostream>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

//Initialisation des variables
int jetons=10;
bool vie=true;

//Fonction roulette
int roulette()
{
    cout << "\n\nVous jouez a la roulette :)" << endl;

    //On d�clare les variables
    int mise=0;
    int nbRoulette=0;
    int pari=0;

    //Tant que le joueur a des jetons et que sont soldes ne d�passe pas 100 jetons
    while((jetons>0)&&(jetons<100))
    {
        //Initalisation du random
        srand(time(NULL));

        //Miser
        cout <<"Vous avez " << jetons << " jetons" << endl << "Entrer une mise comprise entre 1 et 25 en ne depassant pas le nombre de vos jetons disponibles : " << endl;
        cin >>mise;

        //V�rifier la mise
        while ((mise<1) || (mise>jetons)||(mise>25))
        {
            cout <<"ERREUR !!!! Entrer une nouvelle mise : " << endl;
            cin >> mise;
        }

        //Parier
        cout << "\nSur quel nombre ente 0 et 36 pariez vous ? " << endl;
        cin >> pari;

        //V�rifier le pari
        while ((pari<0)||(pari>36))
        {
            cout << "ERREUR !!!! Entrer un autre nombre : " << endl;
            cin >> pari;
        }

        //Randomize nbRoulette
        nbRoulette = rand()%37;
        cout << "Resultat roulette : " << nbRoulette << endl;

        //Perdu si roulette = 0 et pari diff�rent
        if ((nbRoulette==0) && (pari!=0))
        {
            jetons-=mise;
            cout << "\nVous avez perdu !\nNouveau solde : " << jetons << endl;
        }
        else if ((pari%2)==(nbRoulette%2))
                {
                //Gagn�
                jetons+=mise;
                cout << "\nVous avez gagne !\nNouveau solde : " << jetons << endl;
                }
                else
                {
                    //Perdu
                    jetons-=mise;
                    cout << "\nVous avez perdu !\nNouveau solde : " << jetons << endl;
                }
    }

    //Fin de partie roulette
    if (jetons==0) //Fin si vous n'avez plus de jetons
    {
        cout << "\nVous etes fauche ! Maintenant votre vie est en jeu !\n" << endl;
    }
    else if (jetons>=100) //Fin si vous avez gagn�
    {
        cout << "\n\nVous avez sauve votre tete de la mafia ! Bravo !" << endl;
    }
    return jetons;
}

//Fonction roulette russe
int rouletteRusse()
{
    cout << "\n\nVous jouez maintenant a la roulette russe..." << endl;

    //Initalisation du random
    srand(time(NULL));

    //D�claration d'un compteur
    int i;

    //On d�clare le tableau pour le barillet
    int barillet[6];
    for (i=0;i<6;++i)
    {
        barillet[i]=0;
    }

    //On d�clare la balle ainsi que son logement dans le barillet et on lui affecte une valeur diff�rente des autres logements vides
    int balle=rand()%6;
    barillet[balle]=1;

    /*On v�rifie si le coup est partie ou non*/

    //On d�clare les variables
    int choixJoueur=1;
    int nbEssai=0;
    i=0;
    //On oblige le premier tour et on laisse le choix au joueur de continuer ou de retourner � la roulette ensuite
    while ((choixJoueur==1)&&vie)
    {
        //Apr�s le premier essai, le joueur peut choisir la roulette ou continuer la roulette russe
        if (nbEssai>=1)
        {
            cout << "\nVous avez la possiblite de rejouer a la roulette russe ou de revenir a la roulette." << endl << "Entrer 0 pour retourner a la roulette sinon entre 1 : " << endl;
            cin >> choixJoueur;

            //V�rifier le choix
            while (choixJoueur<0||choixJoueur>1)
            {
                cout << "Votre choix est errone ! Entrer un nombre entre 0 ou 1 : " << endl;
                cin >> choixJoueur;
            }

            //Si le joueur choisi la roulette
            if (choixJoueur==0)
            {
                break;
            }
        }

        //Si meurt
        if (barillet[i]==1)
        {
            vie=false;
            cout << "\n\nVous etes mort !" << endl;
        }

        //Si en vie
        else
        {
            jetons+=20;
            cout << "\nOuf ! Vous etes en vie ! Jetons = " << jetons << endl;
            ++i;
            ++nbEssai;
            cout << "Vous avez essayez " << nbEssai << " fois" <<endl;
        }
    }
    return jetons;
}

int main()
{
    //D�but de partie. On demande au joueur de choisir entre la roulette et la roulette russe
    cout << "Vous etes endette de 100 jetons.\nVous devez jouer a la roulette ou a la roulette russe." << endl;

    //La partie continue tant que le joueur est en vie
    while (vie&&(jetons<100))
    {
        //On joue � la roulette
        roulette();

        //On joue � la roulette russe si le joueur perd � la roulette
        if (jetons<100)
        {
            rouletteRusse();
        }
    }
    return 0;
}
